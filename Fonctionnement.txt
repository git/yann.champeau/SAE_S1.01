Section principale :
 - L'application dispose d'un menu principal, vers lequel l'utilisateur est redirigé après chaque action.
 - Il présente les différentes actions possibles, présentées ci-dessous, qui sont les fonctionnalités principales de l'application.
 - À chaque action correspond un code constitué d'un chiffre, utilisé pour sélectionner l'action à réaliser.

Quitter l'application :
 - À la fermeture de l'applicaton, la base de clients chargée dans l'application est sauvegardée pour les prochaines utilisations.
 - Avant de fermer complètement l'application, l'utilisateur doit confirmer la fermeture.
 - Si l'application ne parvient pas à sauvegarder la base de clients, une sécurité l'empêche de s'éteindre d'elle-même tant que le problème n'est pas résolu.

Ajouter un adhérent :
 - Pour ajouter un adhérent, son âge sera demandé.
 - Un identifiant de carte lui sera alors attribué, identifiant qui lui permettra dès lors de s'identifier pour les actions portant sur un adhérent précis.
 - 20 points de bienvenue lui sont offerts.

Supprimer un adhérent :
 - L'identifiant de carte de l'adhérent est demandé afin d'identifier l'adhérent à supprimer.
 - L'adhérent est ensuite supprimé de la base de clients chargée dans l'application.

Afficher le compte d'un adhérent :
 - L'identifiant de carte de l'adhérent est demandé.
 - Ses informations, telles que le total de points à sa disposition, son age, s'il a déjà fréquenté ou non le complexe sportif depuis le lancement de l'application, ainsi le statut d'activation de sa carte sont affichées.

Afficher la liste des adhérents :
 - La liste de tous les adhérents chargée dans l'application, comprenant les modificaions non-enregistrées, s'affiche.
 - S'affichent également les informations complémentaires relatives aux adhérents, comme citées dans --Afficher le compte d'un adhérent--.

Afficher le nombre de séances effectuées par activité dans la journée :
 - La liste des activités proposées par le centre s'affiche.
 - S'affiche également le nombre de séances effectuées depuis le lancement de l'application par activités.

Créditer une carte :
 - L'identifiant de carte de l'adhérent est demandé.
 - Le total de points correspondant à l'adhérent est affiché.
 - Le nombre de points à créditer est demandé.
 - Un réel paiement n'étant pas réalisable, il est demandé à l'utilisateur de compléter un mini-jeu interactif dont l'objectif est d'écrire la lettre proposée.
 - Le nombre de manches correspond au nombre de points à créditer.

Désactiver ou activer une carte :
 - L'identifiant de carte de l'adhérent est demandé.
 - Le statut de la carte est affiché.
 - L'utilisateur choisis s'il veut modifier le statut de la carte. (Activer la carte si elle est désactivée ou inversement.)
 - Le statut de la carte est ensuite modifié ou non.

S'inscrire à une ou plusieures activité(s) :
 - La liste des activités proposées par le centre s'affiche ainsi que leur cout en points et leur identifiant.
 - L'identifiant de carte de l'adhérent est demandé.
 - Le total de points correspondant à l'adhérent est affiché.
 - Le nombre d'activités voulues est demandé.
 - Pour chaque activité, l'identifiant de l'activité voulue est demandé.
 - Si un utilisateur ne dispose pas d'assez de points pour s'inscrire à une activité, la sélection des activités est interrompue.
 - Un âge minimal de 10 ans est requis pour s'inscrire au karting (identifiant : 10).
 - Un point est offert à l'adhérent pour chaque tranche de 15 points dépensés simultanément.
 - Le nombre de points offerts ainsi que de points dépensés est rappelé à l'utilisateur.

Sauvegarder manuellement la base de clients :
 - La base de clients chargée dans l'application est sauvegardée pour les prochaines utilisations.
 - Contrairement à la sauvegarde automatique lors de la fermeture de l'application, l'utilisateur peut continuer d'utiliser l'application après la sauvegarde manuelle.