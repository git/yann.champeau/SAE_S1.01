Application: sae.o main.o
	gcc sae.o main.o -o Application

main.o: main.c sae.h
	gcc -Wall -c main.c

sae.o: sae.c sae.h
	gcc -Wall -c sae.c

clean:
	rm *.o Application

doc: sae.h
	doxygen