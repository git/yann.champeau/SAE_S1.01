Auteurs : Champeau Yann et Baccaud David. 

Lien du git : https://codefirst.iut.uca.fr/git/yann.champeau/SAE_S1.01.git

Pour lancer l'application, tapez simplement "make" !
Pour supprimer les fichiers résultant de la compilation, tapez "make clean".
Pour générer le dossier Doxyfile, tapez "make doc".

Identifiants des activités :
  - Musculation         | ID : 0.
  - Aquagym             | ID : 1.
  - Spa                 | ID : 2.
  - Football en salle   | ID : 3.
  - Badminton           | ID : 4.
  - Tennis de table     | ID : 5.
  - Tennis              | ID : 6.
  - Massage             | ID : 7.
  - Basket              | ID : 8.
  - Volleyball          | ID : 9.
  - Karting             | ID : 10.



Identifiants des fonctionnalités :
  - Arrêter l'application.                                                  | Code d'appel : 0.
  - Ajouter un adhérent.                                                    | Code d'appel : 1.
  - Supprimer un adhérent.                                                  | Code d'appel : 2.
  - Afficher le compte d'un adhérent.                                       | Code d'appel : 3.
  - Afficher la liste des adhérents.                                        | Code d'appel : 4.
  - Afficher le nombre de séances effectuées par activité dans la journée.  | Code d'appel : 5.
  - Créditer une carte.                                                     | Code d'appel : 6.
  - Désactiver ou activer une carte.                                        | Code d'appel : 7.
  - Faire une activité.                                                     | Code d'appel : 8.
  - Sauvegarder manuellement.                                               | Code d'appel : 9.

Attention : l'application est très communicative, toute erreur sera signalée dans la sortie d'erreur. Des messages, moins précis, seront aussi envoyés dans la sortie standard.