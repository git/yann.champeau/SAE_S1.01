#include <stdlib.h>
#include <stdio.h>
#include "sae.h"

#define TMAX 100000
#define TACT 11

int main(void){
  printf("\nL'application démarre ...\n");
  int idCarte[TMAX], points[TMAX], age[TMAX], frequentation[TMAX], actif[TMAX];
  int tlog=0, err, action=-1, secur='N';
  int cout[TACT]={10,15,14,11,7,4,8,15,6,7,22}, nbSeances[TACT];
  char capt;
  err=loadAdherents(idCarte, points, age, frequentation, actif, TMAX, &tlog);
  if(err){
    printf("Echec de chargement du fichier clients.\n");
    return 1;
  }
  while(action){
    printf("La base de clients a bien pu être récupérée.\n\n\n");
    printf("                      #-------------------------------------------------#\n");
    printf("                      |                    Bienvenue                    |\n");
    printf("                      |           sur l'application de gestion          |\n");
    printf("                      |          des clients du complexe sportif.       |\n");
    printf("                      #-------------------------------------------------#\n");
    printf("\n\n#-----------------------------------------------------------------------------------------------#\n");
    printf("\n  - Arrêter l'application.                                                  | Code d'appel : 0.\n  - Ajouter un adhérent.                                                    | Code d'appel : 1.\n  - Supprimer un adhérent.                                                  | Code d'appel : 2.\n  - Afficher le compte d'un adhérent.                                       | Code d'appel : 3.\n  - Afficher la liste des adhérents.                                        | Code d'appel : 4.\n  - Afficher le nombre de séances effectuées par activité dans la journée.  | Code d'appel : 5.\n  - Créditer une carte.                                                     | Code d'appel : 6.\n  - Désactiver ou activer une carte.                                        | Code d'appel : 7.\n  - Faire une activité.                                                     | Code d'appel : 8.\n  - Sauvegarder manuellement la base de clients.                            | Code d'appel : 9.\n");
    printf("\n#-----------------------------------------------------------------------------------------------#\n\nIndiquez le code correspondant à l'action souhaitée :\n\n");
    scanf("%d",&action);
    printf("\n");
    switch(action){
      case 0:
        printf("Etes-vous sûr de vouloir fermer l'application ? (Y pour accepter, n'importe quelle autre touche pour annuler.)\n\n");
        scanf("%*c%c", &secur);
        if(secur!='Y') action=-1;
        system("clear");
        break;
      case 1:
        err=ajoutAdherent(idCarte, points, age, frequentation, actif, TMAX, &tlog);
        if(err) printf("Echec de l'ajout du client.\n");
        else printf("Le client a bien été enregistré.\n");
        reset();
        break;
      case 2:
        err=suppressionAdherent(idCarte, points, age, frequentation, actif, &tlog);
        if(err) printf("Echec de la suppression du client.\n");
        else printf("Le client a bien été supprimé.\n");
        reset();
        break;
      case 3:
        err=affichageCompte(idCarte, points, age, frequentation, actif, tlog);
        if(err) printf("Echec de l'affichage du compte du client.\n");
        reset();
        break;
      case 4:
        affichageListe(idCarte, points, age, frequentation, actif, tlog);
        reset();
        break;
      case 5:
        affichageNbSeances(nbSeances, TACT);
        reset();
        break;
      case 6:
        err=creditation(idCarte, points, actif, tlog);
        if(err) printf("\nEchec de la créditation du compte du client.\n");
        reset();
        break;
      case 7:
        err=gestionCarte(idCarte, actif, tlog);
        if(err) printf("Echec de la modification de la carte.\n");
        reset();
        break;
      case 8:
        err=activite(idCarte, points, age, frequentation, actif, cout, nbSeances, tlog);
        if(err) printf("Echec de l'inscription du client à l'activité.\n");
        reset();
        break;
      case 9:
        err=saveAdherents(idCarte, points, age, actif, tlog);
        if(err)printf("Echec de l'enregistrement de la base de clients.\n");
        else printf("La base de clients a bien été enregistrée.\n");
        reset();
    }
  }
  err=saveAdherents(idCarte, points, age, actif, tlog);
  if(!err){
    printf("La base de clients est sauvegardée.\n");
    printf("L'application se ferme ...\n");
    return 0;
  }
  else{
    printf("Echec de l'enregistrement de la base de clients, veuillez régler le problème puis réessayer en appuyant sur entrer : \n");
    scanf("%*c%c",&capt);
    printf("\n");
    while(saveAdherents(idCarte, points, age, actif, tlog)==1){
      printf("Echec de l'enregistrement de la base de clients, veuillez régler le problème puis réessayer en appuyant sur entrer : \n");
      scanf("%c",&capt);
      printf("\n");
    }
    printf("L'application a réussi à sauvegarder la base de clients.\n");
    printf("L'application se ferme ...\n");
    return 0;
  }
}