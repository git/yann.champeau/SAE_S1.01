#include <stdlib.h>
#include <stdio.h>
#include "sae.h"

int loadAdherents(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tmax, int *tlog){
  FILE *f;
  int i=0, id, pts, ageAdh, act;
  f=fopen("adherents.txt","r");
  if(f==NULL){
    fprintf(stderr,"Erreur de chargement du fichier adhérents.\n");
    return 1;
  }
  fscanf(f,"%d%d%d%d",&id,&pts,&ageAdh,&act);
  while(!feof(f)){
    if(i==tmax){
      fprintf(stderr,"Erreur, trop d'adhérents.\n");
      return 1;
    }
    idCarte[i]=id;
    points[i]=pts;
    age[i]=ageAdh;
    frequentation[i]=0;
    actif[i]=act;
    *tlog+=1;
    i++;
    fscanf(f,"%d%d%d%d",&id,&pts,&ageAdh,&act);
  }
  fclose(f);
  return 0;
}

int recherche(int tabtri[], int tlog, int n, int *pres){
  int i=0;
  while(tabtri[i]<n){
    if(i==tlog){
      *pres=0;
      return -1;
    }
    i++;
  }
  if(tabtri[i]!=n){
    *pres=0;
    return -1;
  }
  *pres=1;
  return i;
}

void suppression(int tab[], int tlog, int n){
  for(int i=n; i<tlog-1;i++){
    tab[i]=tab[i+1];
  }
}

int ajoutAdherent(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tmax, int *tlog){
  if(*tlog==tmax){
    fprintf(stderr,"Erreur, trop d'adhérents.\n");
    return 1;
  }
  int ageAdh, id;
  printf("Indiquez l'âge de l'adhérent : \n\n");
  scanf("%d",&ageAdh);
  printf("\n");
  if(!*tlog) id=1;
  idCarte[*tlog]=idCarte[*tlog-1]+1;
  points[*tlog]=20;
  age[*tlog]=ageAdh;
  frequentation[*tlog]=0;
  actif[*tlog]=1;
  printf("L'identifiant de carte attribué est le : %d.\n\n",idCarte[*tlog-1]+1);
  *tlog+=1;
  return 0;
}

int suppressionAdherent(int idCarte[], int points[], int age[], int frequentation[], int actif[], int *tlog){
  int id;
  if(*tlog==0){
    fprintf(stderr,"Erreur, aucun adhérent.\n");
    return 1;
  }
  printf("Indiquez l'identifiant de carte de l'adhérent : \n\n");
  scanf("%d",&id);
  printf("\n");
  int pres, i;
  i=recherche(idCarte, tlog, id, &pres);
  if(!pres){
    fprintf(stderr, "L'adhérent n'a pas été trouvé\n");
    return 1;
  }
  suppression(idCarte, *tlog,  i);
  suppression(points, *tlog, i);
  suppression(age, *tlog, i);
  suppression(frequentation, *tlog, i);
  suppression(actif, *tlog, i);
  *tlog-=1;
  return 0;
}

int affichageCompte(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tlog){
  int id, pres, i;
  printf("Indiquez l'identifiant de carte de l'adhérent : \n\n");
  scanf("%d",&id);
  printf("\n");
  i=recherche(idCarte, tlog, id, &pres);
  if(!pres){
    fprintf(stderr,"Adhérent non existant !\n");
    return 1;
  }
  printf("Voici l'état actuel du compte de l'adhérent %d :\n\n  - Points : %d.\n  - Âge : %d.\n  - Fréquentation : %d\n  - Carte active : %d\n",idCarte[i], points[i], age[i], frequentation[i], actif[i]);
  return 0;
}

void affichageListe(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tlog){
  printf("Numéro de carte     + Nombre de points +       Âge       +    Fréquentation  +    Carte active\n");
  for(int i=0; i<tlog; i++){
  printf("       %d\t        |        %d\t       |       %d\t     |         %d\t     |        %d\n",idCarte[i],points[i],age[i],frequentation[i],actif[i]);
  }
}

void affichageNbSeances(int nbSeances[], int tact){
  printf("Nom de l'activité   +  Nombre de séances\n");
  printf("  Musculation       |          %d\n  Aquagym           |          %d\n  Spa               |          %d\n  Football en salle |          %d\n  Badminton         |          %d\n  Tennis de table   |          %d\n  Tennis            |          %d\n  Massage           |          %d\n  Basket            |          %d\n  Volleyball        |          %d\n  Karting           |          %d\n\n", nbSeances[0], nbSeances[1], nbSeances[2], nbSeances[3], nbSeances[4], nbSeances[5], nbSeances[6], nbSeances[7], nbSeances[8], nbSeances[9], nbSeances[10]);
}

int creditation(int idCarte[], int points[], int actif[], int tlog){
  int i, idcarte, pts, pres;
  printf("Indiquez l'identifiant de la carte : \n\n");
  scanf("%d", &idcarte);
  i=recherche(idCarte, tlog, idcarte, &pres);
  if(!pres){
    fprintf(stderr,"\nCette carte n'existe pas.\n");
    return 1;
  }
  if(!actif[i]){
    fprintf(stderr,"\nLa carte de l'adhérent n'est pas active.\n");
    return 1;
  }
  printf("\nL'adhérent dispose de %d points sur son compte.\n",points[i]);
  printf("\nCombien de points voulez-vous créditer sur la carte de l'adhérent ?\n\n");
  scanf("%d", &pts);
  printf("\n");
  srand(time(NULL));
  char alph[]="abcdefghijklmnopqrstuvwxyz";
  char l, t=NULL;
  for(int j=0; j<pts; j++){
    l=alph[rand()%25+0];
    while(t!=l){
      printf("Tapez la lettre %c : ",l);
      scanf("%*c%c",&t);
      printf("\n");
    }
    t=NULL;
  }
  points[i]+= pts;
  printf("\nLes points ont bien étés crédités.\n");
  return 0;
}

int gestionCarte(int idCarte[], int actif[], int tlog){
  int i, idcarte, pres;
  char choix=NULL;
  printf("Tapez l'identifiant de la carte en question :\n\n");
  scanf("%d", &idcarte);
  i=recherche(idCarte, tlog, idcarte, &pres);
  if(!pres){
    fprintf(stderr,"\nLa carte n'existe pas\n");
    return 1;
  }
  while(choix!='O'){
    if(actif[i]) printf("\nLa carte est activée, voulez vous la désactiver ? (O/N)\n\n");
    else printf("\nLa carte est désactivée, voulez vous l'activer ? (O/N)\n\n");
    scanf("%*c%c",&choix);
    if(choix=='N'){
      printf("\nAnnulation de la modification de la carte.\n");
      return 0;
    }
  }
  actif[i]=(actif[i]-1)*(actif[i]-1);
  printf("\nLe statut de la carte a bien été modifié.\n");
  return 0;
}

int activite(int idCarte[], int points[], int age[],  int frequentation[], int actif[], int cout[], int nbSeances[], int tlog){
  int i, idAct, idcarte, pres, nbAct, sommeDep=0;
  printf("Identifiants des activités :\n  - Musculation       | 10 pts. | ID : 0.\n  - Aquagym           | 15 pts. | ID : 1.\n  - Spa               | 14 pts. | ID : 2.\n  - Football en salle | 11 pts. | ID : 3.\n  - Badminton         |  7 pts. | ID : 4.\n  - Tennis de table   |  4 pts. | ID : 5.\n  - Tennis            |  8 pts. | ID : 6.\n  - Massage           | 15 pts. | ID : 7.\n  - Basket            |  6 pts. | ID : 8.\n  - Volleyball        |  7 pts. | ID : 9.\n  - Karting           | 22 pts. | ID : 10.\n\n");
  printf("Indiquez l'identifiant de la carte : \n\n");
  scanf("%d", &idcarte);
  i=recherche(idCarte, tlog, idcarte, &pres);
  if(!pres){
    fprintf(stderr,"\nCette carte n'existe pas.\n\n");
    return 1;
  }
  if(!actif[i]){
    fprintf(stderr,"\nLa carte de l'adhérent n'est pas active.\n\n");
    return 1;
  }
  if(frequentation[i]){
    printf("\nL'adhérent a déjà visité le centre aujourd'hui.\n\n");
    return 1;
  }
  printf("\nL'adhérent dispose de %d points sur son compte.\n",points[i]);
  printf("\nCombien d'activités voulez-vous faire aujourd'hui ?\n\n");
  scanf("%d", &nbAct);
  while(nbAct>0){
    printf("\nIndiquez l'identifiant de l'activité : \n\n");
    scanf("%d", &idAct);
    if(idAct!=10||age[i]>10){
      if(points[i] < cout[idAct]){
        fprintf(stderr, "\nL'adhérent n'a pas assez de points\n");
        return 1;
      }
      points[i]-=cout[idAct];
      sommeDep+=cout[idAct];
      frequentation[i]=1;
      nbSeances[idAct]+=1;
      printf("\nL'adhérent est enregistré pour l'activité %d.\n",idAct);
      nbAct-=1;
    }
    else{
      printf("\nL'âge de l'adhérent est trop bas pour cette activité.\n");
    }
  }
  points[i]+=sommeDep/15;
  printf("\nL'adhérent a obtenu %d point(s) supplémentaire(s) pour avoir dépensé %d points en une fois.\n", sommeDep/15, sommeDep);
  return 0;
}

int saveAdherents(int idCarte[], int points[], int age[], int actif[], int tlog){
  FILE *f = fopen("adherents.txt","w");
  if(fopen("adherents.txt","w")==NULL){
    fprintf(stderr,"Erreur d'ouverture du fichier adhérents.\n");
    return 1;
  }
  for(int i=0; i<tlog; i++){
    fprintf(f,"%d %d %d %d\n", idCarte[i], points[i], age[i], actif[i]);
  }
  fclose(f);
  return 0;
}

void reset(void){
  char capt;
  printf("\n\nAppuyez sur entrer pour continuer :\n");
  scanf("%*c%c",&capt);
  system("clear");
}