/**
*\file sae.h
*\brief Liste des fonctions utilisées dans main.c et sae.c
*\author Baccaud.D Champeau.Y
*\date 11 novembre 2022
*
*Fichier regroupant les prototypes des fonctions utilisées par l'application.
*/


/*Conception de l'application :

  - 1:*/
/**
* \brief ajoute un adhérent aux tableaux contenant la base de clients.
* \param [in,out] idCarte Tableau contenant les identifiants des adhérents.
* \param [in,out] points Tableau contenant les points de chaque adhérent.
* \param [in,out] age Tableau contenant les âges des adhérents.
* \param [in,out] frequentation Tableau contenant la fréquentation de l'adhérent dans la journée. (0 : l'adhérent n'a pas encore fréquenté le centre aujourd'hui, 1 : l'adhérent a déjà fréquenté le centre aujourd'hui.)
* \param [in,out] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tmax Constante représentant la taille physique des tableaux.
* \param [in,out] *tlog Pointeur menant à la taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int ajoutAdherent(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tmax, int *tlog); //Champeau Yann.
/*
  - 2:*/
/**
* \brief supprime un adhérent des tableaux contenant la base de clients.
* \param [in,out] idCarte Tableau contenant les identifiants des adhérents.
* \param [in,out] points Tableau contenant les points de chaque adhérent.
* \param [in,out] age Tableau contenant les âges des adhérents.
* \param [in,out] frequentation Tableau contenant la fréquentation de l'adhérent dans la journée. (0 : l'adhérent n'a pas encore fréquenté le centre aujourd'hui, 1 : l'adhérent a déjà fréquenté le centre aujourd'hui.)
* \param [in,out] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in,out] *tlog Pointeur menant à la taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int suppressionAdherent(int idCarte[], int points[], int age[], int frequentation[], int actif[], int *tlog); //Baccaud David, Champeau Yann.
/*
  - 3:*/
/**
* \brief affiche les informations concernant un adhérent. 
* \param [in] idCarte Tableau contenant les identifiants des adhérents.
* \param [in] points Tableau contenant les points de chaque adhérent.
* \param [in] age Tableau contenant les âges des adhérents.
* \param [in] frequentation Tableau contenant la fréquentation de l'adhérent dans la journée. (0 : l'adhérent n'a pas encore fréquenté le centre aujourd'hui, 1 : l'adhérent a déjà fréquenté le centre aujourd'hui.)
* \param [in] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tlog taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int affichageCompte(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tlog); // Baccaud David, Champeau Yann.

/*
  - 4:*/
/**
* \brief affiche la liste de tous les adhérents. 
* \param [in] idCarte Tableau contenant les identifiants des adhérents.
* \param [in] points Tableau contenant les points de chaque adhérent.
* \param [in] age Tableau contenant les âges des adhérents.
* \param [in] frequentation Tableau contenant la fréquentation de l'adhérent dans la journée. (0 : l'adhérent n'a pas encore fréquenté le centre aujourd'hui, 1 : l'adhérent a déjà fréquenté le centre aujourd'hui.)
* \param [in] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tlog taille logique des tableaux. 
*/
void affichageListe(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tlog); // Baccaud David, Champeau Yann.
/*
  - 5:*/
/**
* \brief affiche le nombre de séances effectuées pour chaque activité. 
* \param [in] nbSeances Tableau contenant le nombre de séance pour chaque activité.
* \param [in] tact variable représentant la taille maximum du tableau nbSeances.
*/
void affichageNbSeances(int nbSeances[], int tact); // Baccaud David, Champeau Yann.
/*
  - 6:*/
/**
* \brief permet de rajouter des points sur sa carte par le biais d'un mini-jeu.
* \param [in] idCarte Tableau contenant les identifiants des adhérents.
* \param [in,out] points Tableau contenant les points de chaque adhérent.
* \param [in] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tlog taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int creditation(int idCarte[], int points[], int actif[], int tlog); // Baccaud David, Champeau Yann.
/*
  - 7:*/
/**
* \brief permet d'activer une carte ou de la désactiver.  
* \param [in] idCarte Tableau contenant les identifiants des adhérents.
* \param [in,out] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tlog taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int gestionCarte(int idCarte[], int actif[], int tlog); // Champeau Yann, Baccaud David.
/*
  - 8:*/
/**
* \brief permet d'inscrire un adhérent à une ou plusieures activités et lui retire le nombre de points correspondant.
* \param [in] idCarte Tableau contenant les identifiants des adhérents.
* \param [in,out] points Tableau contenant les points de chaque adhérent.
* \param [in] age Tableau contenant les âges des adhérents.
* \param [in,out] frequentation Tableau contenant la fréquentation de l'adhérent dans la journée. (0 : l'adhérent n'a pas encore fréquenté le centre aujourd'hui, 1 : l'adhérent a déjà fréquenté le centre aujourd'hui.)
* \param [in] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] cout Tableau contenant les couts en points des activités.
* \param [in,out] nbSeances Tableau contenant le nombre de séances déja effectuées le même jour par activités.
* \param [in] tlog taille logique des tableaux concernant les adhérents.
* \return 1 en cas d'erreur, 0 sinon.
*/
int activite(int idCarte[], int points[], int age[], int frequentation[], int actif[], int cout[], int nbSeances[], int tlog); // Baccaud David, Champeau Yann.
/*
  - 9:*/
/**
* \brief sauvegarde la base de clients contenue dans les tableaux donnés en paramètre dans le fichier \b adherents.txt.
* \param [in] idCarte Tableau contenant les identifiants des adhérents.
* \param [in] points Tableau contenant les points de chaque adhérent.
* \param [in] age Tableau contenant les âges des adhérents.
* \param [in] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tlog taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int saveAdherents(int idCarte[], int points[], int age[], int actif[], int tlog); //Champeau Yann.
/*

Autres fonctions :*/

/**
* \brief charge la base de clients stockée dans le fichier \b adherents.txt dans les tableaux donnés en paramètre.
* \param [out] idCarte Tableau contenant les identifiants des adhérents.
* \param [out] points Tableau contenant les points de chaque adhérent.
* \param [out] age Tableau contenant les âges des adhérents.
* \param [out] frequentation Tableau contenant la fréquentation de l'adhérent dans la journée. (0 : l'adhérent n'a pas encore fréquenté le centre aujourd'hui, 1 : l'adhérent a déjà fréquenté le centre aujourd'hui.)
* \param [out] actif Tableau indiquant si la carte est active. (0 : la carte n'est pas active, 1 : la carte est active.)
* \param [in] tmax Constante représentant la taille physique des tableaux.
* \param [out] *tlog Pointeur menant à la taille logique des tableaux.
* \return 1 en cas d'erreur, 0 sinon.
*/
int loadAdherents(int idCarte[], int points[], int age[], int frequentation[], int actif[], int tmax, int *tlog); //Baccaud David, Champeau Yann.

/**
* \brief recherche un élément dans un tableau trié.
* \param [in] tabtri Tableau trié dans lequel on cherche \b n.
* \param [in] tlog taille logique du tableau \b tab.
* \param [in] n élément à rechercher dans \b tabtri.
* \param [out] pres variable permettant de communiquer la présence ou non-présence de l'élément cherché dans le tableau \b tabtri.
* \return l'indice de \b n dans \b tabtri, -1 sinon.
*/
int recherche(int tabtri[], int tlog, int n, int * pres); //Baccaud David, Champeau Yann.

/**
* \brief supprime la ligne d'indice \b n dans le tableau \b tab.
* \param [in,out] tab Tableau contenant la ligne à supprimer.
* \param [in] tlog taille logique du tableau \b tab.
* \param [in] n indice de la ligne à supprimer.
*/
void suppression(int tab[], int tlog, int n); //Champeau Yann.

/**
* \brief réinitialise l'affichage.
*/
void reset(void); //Champeau Yann.